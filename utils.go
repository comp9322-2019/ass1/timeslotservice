package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

func SendResponse(c *gin.Context, objectToRespond interface{}, code int) {
	bodyBytes, _ := json.Marshal(objectToRespond)
	c.Header("Content-Length", strconv.Itoa(len(bodyBytes)))
	c.Data(code, JsonByteStreamHeader, bodyBytes)
}

func OpenDBConnection(DBUser string, DBPassword string, DBName string, DBHost string) (*sql.DB, error) {
	dbInfo := fmt.Sprintf("%s:%s@tcp(%s)/%s", DBUser, DBPassword, DBHost, DBName)
	return sql.Open("mysql", dbInfo)
}

type ErrorResponse struct {
	Error string
}

func ReturnFailure(c *gin.Context, code int, err error) {
	errToReturn := new(ErrorResponse)
	errToReturn.Error = err.Error()
	sendBytes, _ := json.Marshal(errToReturn)
	c.Header("Content-Length", strconv.Itoa(len(sendBytes)))
	c.Data(code, JsonByteStreamHeader, sendBytes)
}
