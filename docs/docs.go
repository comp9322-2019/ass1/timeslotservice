package docs

import (
	"bytes"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
  "basePath": "/",
  "info": {
    "contact": {
      "email": "z5061594@student.unsw.edu.au",
      "name": "API Support"
    },
    "description": "Service to make bookings and find availible times",
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "title": "Timeslot service",
    "version": "1.0"
  },
  "paths": {
    "/booking": {
      "get": {
        "consumes": [
          "application/json"
        ],
        "description": "Get all bookings, optionally filtering by doctor or start/endtimes. Returns a map of doctorID to booking map",
        "parameters": [
          {
            "description": "Bound the returned bookings by start time",
            "in": "query",
            "name": "startTime",
            "type": "string"
          },
          {
            "description": "Bound the returned bookings by end time",
            "in": "query",
            "name": "endTime",
            "type": "string"
          },
          {
            "description": "Return only bookings for this doctor",
            "in": "query",
            "name": "doctorID",
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DoctorIDtoBookingMap",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Get all bookings",
        "tags": [
          "Booking"
        ]
      },
      "post": {
        "consumes": [
          "application/json"
        ],
        "description": "Create a booking by providing booking info and timeslot information",
        "parameters": [
          {
            "description": "Timeslot and Booking information that should be assigned to the booking",
            "in": "body",
            "name": "BookingData",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NewBookingData",
              "type": "object"
            }
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Booking",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Create a booking",
        "tags": [
          "Booking"
        ]
      }
    },
    "/booking/{id}": {
      "delete": {
        "consumes": [
          "application/json"
        ],
        "description": "Deletes a booking, allowing the times to be rebooked",
        "parameters": [
          {
            "description": "Booking ID to be deleted",
            "in": "path",
            "name": "id",
            "required": true,
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DeleteBookingResponse",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Delete a booking",
        "tags": [
          "Booking"
        ]
      },
      "put": {
        "consumes": [
          "application/json"
        ],
        "description": "Updates the booking information of the given ID, creates a booking at that ID if one did not exist",
        "parameters": [
          {
            "description": "Booking ID to be modified",
            "in": "path",
            "name": "ID",
            "required": true,
            "type": "string"
          },
          {
            "description": "Timeslot and Booking information that should be updated to the booking",
            "in": "body",
            "name": "BookingData",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NewBookingData",
              "type": "object"
            }
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Booking",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Update a booking",
        "tags": [
          "Booking"
        ]
      }
    },
    "/timeslot": {
      "get": {
        "consumes": [
          "application/json"
        ],
        "description": "Returns the availible timeslots between the given start and end times. Defaults the start and end times to tomorrow 9-5",
        "parameters": [
          {
            "description": "Bound the returned timeslots by start time (default 9am tomorrow)",
            "in": "query",
            "name": "startTime",
            "type": "string"
          },
          {
            "description": "Bound the returned timeslots by end time (default 5pm tomorrow)",
            "in": "query",
            "name": "endTime",
            "type": "string"
          },
          {
            "description": "Return only timeslots for this doctor (default show all)",
            "in": "query",
            "name": "doctorID",
            "type": "string"
          },
          {
            "description": "Length of the requested timeslots (must be divisible by 30 minutes)",
            "in": "query",
            "name": "interval",
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DoctorIDtoTimeslotMap",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Get availible timeslots",
        "tags": [
          "Timeslot"
        ]
      }
    }
  },
  "swagger": "2.0",
  "definitions": {
    "ErrorResponse": {
      "type": "object",
      "properties": {
        "Error": {
          "type": "string"
        }
      }
    },
    "Timeslot": {
      "type": "object",
      "properties": {
        "DoctorID": {
          "type": "integer",
          "format": "int64"
        },
        "StartTime": {
          "type": "integer",
          "format": "int64"
        },
        "EndTime": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "BookingInfo": {
      "type": "object",
      "properties": {
        "ClientName": {
          "type": "string"
        },
        "Location": {
          "type": "string"
        },
        "Reason": {
          "type": "string"
        }
      }
    },
    "BookingMeta": {
      "type": "object",
      "properties": {
        "ID": {
          "type": "integer",
          "format": "int64"
        },
        "CreatedAt": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Booking": {
      "allOf": [
        {
          "$ref": "#/definitions/BookingInfo"
        },
        {
          "$ref": "#/definitions/BookingMeta"
        },
        {
          "$ref": "#/definitions/Timeslot"
        }
      ]
    },
    "NewBookingData": {
      "allOf": [
        {
          "$ref": "#/definitions/BookingInfo"
        },
        {
          "$ref": "#/definitions/Timeslot"
        }
      ]
    },
    "DeleteBookingResponse": {
      "type": "object",
      "properties": {
        "DeletedID": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "DoctorIDtoBookingMap": {
      "type": "object",
      "additionalProperties": {
        "$ref": "#/definitions/Booking"
      }
    },
    "DoctorIDtoTimeslotMap": {
      "type": "object",
      "additionalProperties": {
        "$ref": "#/definitions/Timeslot"
      }
    }
  }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo swaggerInfo

type s struct{}

func (s *s) ReadDoc() string {
	t, err := template.New("swagger_info").Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, SwaggerInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
