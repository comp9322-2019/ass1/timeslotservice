package main

import (
	"errors"
	"fmt"
	"time"
)

type Booking struct {
	BookingMeta
	BookingInfo
	Timeslot
}

type BookingMeta struct {
	ID        int64
	CreatedAt int64
}

type BookingInfo struct {
	ClientName string
	Location   string
	Reason     string
}

type Timeslot struct {
	DoctorID  int64
	StartTime int64
	EndTime   int64
}

func (g *BookingInfo) IsNull() bool {
	return g.ClientName == "" && g.Location == "" && g.Reason == ""
}

func (g *Timeslot) IsNull() bool {
	return g.DoctorID == 0 && g.StartTime == 0 && g.EndTime == 0
}

func (g *Timeslot) Valid() bool {
	start := time.Unix(g.StartTime, 0)
	end := time.Unix(g.EndTime, 0)

	doctor, err := GetDoctor(g.DoctorID)

	if err != nil || doctor == nil {
		return false
	}

	return start.Minute()%30 == 0 && end.Minute()%30 == 0
}

func (g *BookingInfo) Valid() bool {
	return g.ClientName != "" && g.Location != "" && g.Reason != ""
}

func (g *Booking) IsNull() bool {
	return g.ID == 0
}

func Possible(info Timeslot) (bool, error) {
	// to figure out if possible, first see if the given timeslot is in buisness hours entirely

	start := time.Unix(info.StartTime, 0)
	end := time.Unix(info.EndTime, 0)

	//start and end must be entirely within the business hours
	if !isBusinessHour(start.Hour()) || !isBusinessHour(end.Hour()) {
		return false, nil
	}

	if start.Day() != end.Day() {
		return false, nil // cannot have day long appointments due to buisness hours
	}

	// now check if any bookings exist in the gap, ie

	values, dbErr := db.Query("SELECT ID FROM Bookings WHERE (?<EndTime AND StartTime<? AND DoctorID=?) OR (StartTime =? AND EndTime =? and DoctorID=?)", info.StartTime, info.EndTime, info.DoctorID, info.StartTime, info.EndTime, info.DoctorID)

	if dbErr != nil {
		return false, dbErr
	}

	checkID := new(int64)
	values.Next()
	values.Scan(&checkID)

	return *checkID == 0, nil
}

func isBusinessHour(hour int) bool {
	return hour >= 9 && hour <= 17
}

func CreateBooking(info BookingInfo, scheduling Timeslot) (*Booking, error) {
	possible, posErr := Possible(scheduling)

	if posErr != nil {
		return nil, posErr
	}

	if !possible {
		return nil, errors.New("Not possible")
	}

	if !scheduling.Valid() {
		return nil, errors.New("Invalid times given")
	}

	if !info.Valid() {
		return nil, errors.New("Invalid info given")
	}

	d := new(Booking)
	currTime := time.Now().Unix()
	exec, initialExecErr := db.Exec("INSERT INTO Bookings (DoctorID, ClientName, Location, Reason, StartTime, EndTime, CreatedAt) VALUES (?,?,?,?,?,?,?)", scheduling.DoctorID, info.ClientName, info.Location, info.Reason, scheduling.StartTime, scheduling.EndTime, currTime)

	if initialExecErr != nil {
		return nil, initialExecErr
	}

	id, idErr := exec.LastInsertId()

	if idErr != nil {
		return nil, idErr // this needs to be fully fleshed
	}

	d.ID = id
	d.CreatedAt = currTime
	d.BookingInfo = info
	d.Timeslot = scheduling

	return d, nil
}

func ModifyBookingInformation(existingTimselot Booking, newInfo BookingInfo) (*Booking, error) {
	if existingTimselot.IsNull() {
		return nil, errors.New("Cannot modify a nil timeslot")
	}

	if !newInfo.Valid() {
		return nil, errors.New("Invalid info")
	}

	toInsertTimeslot := new(Booking)

	if newInfo.ClientName != "" {
		toInsertTimeslot.ClientName = newInfo.ClientName
	} else {
		toInsertTimeslot.ClientName = existingTimselot.ClientName
	}

	if newInfo.Location != "" {
		toInsertTimeslot.Location = newInfo.Location
	} else {
		toInsertTimeslot.Location = existingTimselot.Location
	}

	if newInfo.Reason != "" {
		toInsertTimeslot.Reason = newInfo.Reason
	} else {
		toInsertTimeslot.Reason = existingTimselot.Reason
	}

	toInsertTimeslot.BookingMeta = existingTimselot.BookingMeta
	toInsertTimeslot.Timeslot = existingTimselot.Timeslot

	_, execErr := db.Exec("UPDATE Bookings SET ClientName=?, Location=?, Reason=? WHERE ID=?", toInsertTimeslot.ClientName, toInsertTimeslot.Location, toInsertTimeslot.Reason, existingTimselot.ID)

	if execErr != nil {
		return nil, execErr
	}

	return toInsertTimeslot, nil
}

func ModifyBookingTimeslot(existingTimeslot Booking, newScheduling Timeslot) (*Booking, error) {
	if existingTimeslot.IsNull() {
		return nil, errors.New("Cannot modify a nil timeslot")
	}

	if !newScheduling.Valid() {
		return nil, errors.New("Invalid scheduling")
	}

	toInsertTimeslot := new(Booking)

	if newScheduling.DoctorID != 0 {
		toInsertTimeslot.DoctorID = newScheduling.DoctorID
	} else {
		toInsertTimeslot.DoctorID = existingTimeslot.DoctorID
	}

	if newScheduling.StartTime != 0 {
		toInsertTimeslot.StartTime = newScheduling.StartTime
	} else {
		toInsertTimeslot.StartTime = existingTimeslot.StartTime
	}

	if newScheduling.EndTime != 0 {
		toInsertTimeslot.EndTime = newScheduling.EndTime
	} else {
		toInsertTimeslot.EndTime = existingTimeslot.EndTime
	}

	toInsertTimeslot.BookingInfo = existingTimeslot.BookingInfo
	toInsertTimeslot.BookingMeta = existingTimeslot.BookingMeta

	possible, posErr := Possible(toInsertTimeslot.Timeslot)

	if posErr != nil {
		return nil, posErr
	}
	if !possible {
		return nil, errors.New("Cannot accomadate new scheduling")
	}

	_, execErr := db.Exec("UPDATE Bookings SET DoctorID=?, StartTime=?, EndTime=? WHERE ID=?", toInsertTimeslot.DoctorID, toInsertTimeslot.StartTime, toInsertTimeslot.EndTime, existingTimeslot.ID)

	if execErr != nil {
		return nil, execErr
	}

	return toInsertTimeslot, nil
}

func GetAvailibleTimeslots(startTime int64, endTime int64, doctorIDs []int64, interval int64) (map[int64][]*Timeslot, error) {
	// round the start time to the next minimum Interval block
	start := time.Unix(startTime, 0)
	end := time.Unix(endTime, 0)
	adjustedStart := adjustStartTime(start)

	intervalString := fmt.Sprintf("%dm", interval)
	intervalDuration, intervalErr := time.ParseDuration(intervalString)

	if intervalErr != nil {
		return nil, intervalErr
	}

	toReturn := make(map[int64][]*Timeslot)

	for _, val := range doctorIDs {
		availibilities, err := getAvailibleTimeslotsForDoctor(adjustedStart, intervalDuration, end, val)
		if err != nil {
			fmt.Println(err)
			continue
		}

		toReturn[val] = availibilities
	}

	return toReturn, nil
}

func adjustStartTime(start time.Time) time.Time {
	hour, min, _ := start.Clock()

	// round our hours and minutes
	if (min % 30) != 0 {
		if min < 30 {
			min = 30
		} else {
			min = 0
			hour += 1
		}
	}

	return time.Date(start.Year(), start.Month(), start.Day(), hour, min, 0, 0, timezone)
}

func getAvailibleTimeslotsForDoctor(startTime time.Time, interval time.Duration, endTime time.Time, doctorID int64) ([]*Timeslot, error) {
	toReturn := make([]*Timeslot, 0)

	for movingStart := startTime; movingStart.Before(endTime); movingStart = movingStart.Add(interval) {

		// construct a schedule, see if its possible, add to the current doctor
		sched := new(Timeslot)
		sched.StartTime = movingStart.Unix()
		sched.EndTime = movingStart.Add(interval).Unix()
		sched.DoctorID = doctorID

		possible, posErr := Possible(*sched)

		if posErr != nil {
			return nil, posErr
		}

		if possible {
			toReturn = append(toReturn, sched)
		}
	}

	return toReturn, nil
}

func GetBookings(startTime int64, endTime int64, doctorIDs []int64) (map[int64][]*Booking, error) {
	toReturn := make(map[int64][]*Booking)

	for _, val := range doctorIDs {
		bookings, err := GetBookingsByDoctor(startTime, endTime, val)
		if err != nil {
			continue
		}
		toReturn[val] = bookings
	}

	return toReturn, nil
}

func GetBooking(ID int64) (*Booking, error) {
	toReturn := new(Booking)

	values, dbErr := db.Query("SELECT ID, CreatedAt, ClientName, Location, Reason, DoctorID, StartTime, EndTime from Bookings where ID=?", ID)
	values.Next()

	if dbErr != nil {
		return toReturn, dbErr
	}
	values.Scan(&toReturn.ID, &toReturn.CreatedAt, &toReturn.ClientName, &toReturn.Location, &toReturn.Reason, &toReturn.DoctorID, &toReturn.StartTime, &toReturn.EndTime)

	if toReturn.IsNull() {
		return nil, nil
	}

	return toReturn, nil
}

func GetBookingsByDoctor(startTime int64, endTime int64, doctorID int64) ([]*Booking, error) {
	var toReturn []*Booking

	// just get everything for the given doctor
	values, dbErr := db.Query("SELECT ID, CreatedAt, ClientName, Location, Reason, DoctorID, StartTime, EndTime from Bookings where DoctorID=? AND StartTime>=? AND EndTime<=?", doctorID, startTime, endTime)

	if dbErr != nil {
		return nil, dbErr
	}

	for values.Next() {
		newObject := new(Booking)
		values.Scan(&newObject.ID, &newObject.CreatedAt, &newObject.ClientName, &newObject.Location, &newObject.Reason, &newObject.DoctorID, &newObject.StartTime, &newObject.EndTime)

		toReturn = append(toReturn, newObject)
	}

	return toReturn, nil
}

func DeleteBooking(ID int64) error {
	_, execErr := db.Exec("DELETE FROM Bookings where ID=?", ID)

	return execErr
}
