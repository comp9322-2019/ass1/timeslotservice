package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/alecthomas/template"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"github.com/swaggo/swag"
	"math"
	"strconv"
	"time"
)

const JsonByteStreamHeader = "application/json; charset=utf-8"

var db *sql.DB
var timezone *time.Location

// @title Timeslot service
// @version 1.0
// @description Service to make bookings and find available times

// @contact.name API Support
// @contact.email z5061594@student.unsw.edu.au

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /

func main() {
	//Set up our router
	var err error
	db, err = OpenDBConnection("root", "12345678", "timeslotService", "assignment-cluster.cluster-c0wudp930tjr.ap-southeast-2.rds.amazonaws.com")
	if err != nil {
		fmt.Println(err)
	}

	timezone, err = time.LoadLocation("Australia/Sydney")
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	r := gin.Default()
	r.POST("/booking", PostBookingAPI)
	r.GET("/booking", GetBookingsAPI)   // returns the bookings
	r.GET("/timeslot", GetTimeslotsAPI) // returns timeslots that are availible

	r.GET("/booking/:bookingID", GetBookingAPI)
	r.PUT("/booking/:bookingID", PutBookingAPI)
	r.DELETE("/booking/:bookingID", DeleteBookingAPI)

	config := &ginSwagger.Config{
		URL: "http://localhost:5000/swagger/doc.json", //The url pointing to API definition
	}
	// use ginSwagger middleware to
	r.GET("/swagger/*any", ginSwagger.CustomWrapHandler(config, swaggerFiles.Handler))

	r.Run(":5000") // listen and serve
}

type PostBookingRequest struct {
	BookingInfo `json:"BookingInfo"`
	Timeslot    `json:"Timeslot"`
}

// PostBookingAPI godoc
// @Summary Create a booking
// @Description Create a booking by providing booking info and timeslot information
// @Tags Booking
// @Accept  json
// @Produce  json
// @Param BookingInfo body BookingInfo true "Booking information"
// @Param Timeslot body Timeslot true "Timeslot that the booking should take"
// @Success 200 {object} Booking
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /booking [post]
func PostBookingAPI(c *gin.Context) {
	decoded := new(PostBookingRequest)
	jsonDecodeErr := json.NewDecoder(c.Request.Body).Decode(&decoded)

	if jsonDecodeErr != nil {
		ReturnFailure(c, 400, jsonDecodeErr)
		return
	}

	if !decoded.Timeslot.Valid() {
		ReturnFailure(c, 400, errors.New("Times or Doctor not valid"))
		return
	}

	if !decoded.BookingInfo.Valid() {
		ReturnFailure(c, 400, errors.New("Booking information must be filled out"))
		return
	}

	possible, posErr := Possible(decoded.Timeslot)

	if posErr != nil {
		ReturnFailure(c, 500, posErr)
		return
	}

	if !possible {
		ReturnFailure(c, 400, errors.New("Those times are not availible"))
		return
	}

	createdBooking, err := CreateBooking(decoded.BookingInfo, decoded.Timeslot)

	if err != nil {
		ReturnFailure(c, 500, err)
	} else {
		SendResponse(c, createdBooking, 200)
	}
}

// GetBookingsAPI godoc
// @Summary Get all bookings
// @Description Get all bookings, optionally filtering by doctor or start/endtimes. Returns a map of doctorID to booking array
// @Tags Booking
// @Accept  json
// @Produce  json
// @Param startTime query string false "Bound the returned bookings by start time"
// @Param endTime query string false "Bound the returned bookings by end time"
// @Param doctorID query string false "Return only bookings for this doctor"
// @Success 200 {object} map[int64][]*Booking
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /booking [get]
func GetBookingsAPI(c *gin.Context) {
	startTime, startExists := c.GetQuery("startTime")

	var startTimeParsed int64
	var errParsed error

	if startExists {
		startTimeParsed, errParsed = strconv.ParseInt(startTime, 10, 64)

		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("Start time malformed"))
		}
	} else {
		startTimeParsed = 0
	}

	endTime, endExists := c.GetQuery("endTime")

	var endTimeParsed int64

	if endExists {
		endTimeParsed, errParsed = strconv.ParseInt(endTime, 10, 64)

		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("End time malformed"))
		}
	} else {
		endTimeParsed = math.MaxInt64
	}

	doctorIDs, doctorIDsExists := c.GetQuery("doctorID")

	var doctorIDParsed []int64
	if doctorIDsExists {
		doctorIDParsed = make([]int64, 1)
		doctorIDParsed[0], errParsed = strconv.ParseInt(doctorIDs, 10, 64)

		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("Doctor ID malformed"))
		}
	} else {
		IDs, IDErr := GetDoctorIDs()

		if IDErr != nil {
			ReturnFailure(c, 500, IDErr)
			return
		}

		doctorIDParsed = IDs
	}
	toReturn, err := GetBookings(startTimeParsed, endTimeParsed, doctorIDParsed)

	if err != nil {
		ReturnFailure(c, 500, err)
	} else {
		SendResponse(c, toReturn, 200)
	}
}

// GetTimeslotsAPI godoc
// @Summary Get availible timeslots
// @Description Returns the availible timeslots between the given start and end times. Defaults the start and end times to tomorrow 9-5
// @Tags Timeslot
// @Accept  json
// @Produce  json
// @Param startTime query string false "Bound the returned timeslots by start time (default 9am tomorrow)"
// @Param endTime query string false "Bound the returned timeslots by end time (default 5pm tomorrow)"
// @Param doctorID query string false "Return only timeslots for this doctor (default show all)"
// @Param interval query string false "Length of the requested timeslots (must be divisible by 30 minutes)"
// @Success 200 {object} map[int64][]*Timeslot
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /timeslot [get]
func GetTimeslotsAPI(c *gin.Context) {
	now := time.Now()
	// get list of doctor ids, default to the full list
	// get interval from query params, default to 30

	startTime, startExists := c.GetQuery("startTime")

	var startTimeParsed int64
	var errParsed error

	if startExists {
		startTimeParsed, errParsed = strconv.ParseInt(startTime, 10, 64)

		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("Start time malformed"))
		}
	} else {
		startTimeParsed = time.Date(now.Year(), now.Month(), now.Day()+1, 9, 0, 0, 0, timezone).Unix()
	}

	endTime, endExists := c.GetQuery("endTime")

	var endTimeParsed int64

	if endExists {
		endTimeParsed, errParsed = strconv.ParseInt(endTime, 10, 64)

		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("End time malformed"))
		}
	} else {
		endTimeParsed = time.Date(now.Year(), now.Month(), now.Day()+1, 17, 0, 0, 0, timezone).Unix()
	}

	doctorIDs, doctorIDsExists := c.GetQuery("doctorID")

	var doctorIDParsed []int64
	if doctorIDsExists {
		doctorIDParsed = make([]int64, 1)
		doctorIDParsed[0], errParsed = strconv.ParseInt(doctorIDs, 10, 64)

		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("Doctor ID malformed"))
		}
	} else {
		IDs, IDErr := GetDoctorIDs()

		if IDErr != nil {
			ReturnFailure(c, 500, IDErr)
			return
		}

		doctorIDParsed = IDs
	}

	interval, intervalExists := c.GetQuery("interval")

	var intervalParsed int64
	if intervalExists {
		intervalParsed, errParsed = strconv.ParseInt(interval, 10, 64)
		if errParsed != nil {
			ReturnFailure(c, 400, errors.New("Interval malformed"))
		}
	} else {
		intervalParsed = 30
	}

	toReturn, err := GetAvailibleTimeslots(startTimeParsed, endTimeParsed, doctorIDParsed, intervalParsed)

	if err != nil {
		ReturnFailure(c, 500, err)
	} else {
		SendResponse(c, toReturn, 200)
	}
}

type DeleteBookingResponse struct {
	DeletedID int64
}

// DeleteBookingAPI godoc
// @Summary Delete a booking
// @Description Deletes a booking, allowing the times to be rebooked
// @Tags Booking
// @Accept  json
// @Produce  json
// @Param ID path string true "Booking ID to be deleted"
// @Success 200 {object} DeleteBookingResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /booking/{id} [delete]
func DeleteBookingAPI(c *gin.Context) {
	booking, err := validateGivenParams(c)

	if err != nil {
		return
	}

	deleteErr := DeleteBooking(booking.ID)

	if deleteErr != nil {
		ReturnFailure(c, 500, deleteErr)
		return
	}

	toReturn := new(DeleteBookingResponse)
	toReturn.DeletedID = booking.ID

	SendResponse(c, toReturn, 200)
}

// GetBookingAPI godoc
// @Summary Get a specific booking
// @Description Returns the information corresponding to a specific booking ID
// @Tags Booking
// @Accept  json
// @Produce  json
// @Param ID path string true "Booking ID to have information returned on"
// @Success 200 {object} Booking
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /booking/{id} [get]
func GetBookingAPI(c *gin.Context) {
	booking, err := validateGivenParams(c)

	if err == nil {
		SendResponse(c, booking, 200)
	}
}

type putRequestBody struct {
	BookingInfo
	Timeslot
}

// PutBookingAPI godoc
// @Summary Update a booking
// @Description Updates the booking information of the given ID, creates a booking at that ID if one did not exist
// @Tags Booking
// @Accept  json
// @Produce  json
// @Param ID path string true "Booking ID to be modified"
// @Param BookingInfo body BookingInfo true "Booking information"
// @Param Timeslot body Timeslot true "Timeslot information"
// @Success 200 {object} Booking
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /booking/{id} [put]
func PutBookingAPI(c *gin.Context) {
	existingBooking, err := validateGivenParams(c)

	if err != nil {
		return
	}

	decoded := new(putRequestBody)
	jsonDecodeErr := json.NewDecoder(c.Request.Body).Decode(&decoded)

	if jsonDecodeErr != nil {
		ReturnFailure(c, 400, jsonDecodeErr)
		return
	}

	if !decoded.Timeslot.Valid() {
		ReturnFailure(c, 400, errors.New("Times or doctor not valid"))
		return
	}

	if !decoded.BookingInfo.Valid() {
		ReturnFailure(c, 400, errors.New("Booking information must be filled out"))
	}

	possible, posErr := Possible(decoded.Timeslot)

	if posErr != nil {
		ReturnFailure(c, 500, posErr)
		return
	}

	if !possible {
		ReturnFailure(c, 400, errors.New("Those times are not availible"))
		return
	}

	newBooking := existingBooking

	if !decoded.BookingInfo.IsNull() {
		newExistingBooking, modifyInfoErr := ModifyBookingInformation(*newBooking, decoded.BookingInfo)

		if modifyInfoErr != nil {
			ReturnFailure(c, 500, modifyInfoErr)
			return
		}

		newBooking = newExistingBooking
	}

	if !decoded.Timeslot.IsNull() {
		newExistingBooking, modifyInfoErr := ModifyBookingTimeslot(*newBooking, decoded.Timeslot)

		if modifyInfoErr != nil {
			ReturnFailure(c, 500, modifyInfoErr)

			//attempt to roll back
			ModifyBookingInformation(*newBooking, existingBooking.BookingInfo)
			return
		}

		newBooking = newExistingBooking
	}

	SendResponse(c, newBooking, 200)
}

func validateGivenParams(c *gin.Context) (*Booking, error) {
	stringID := c.Param("bookingID")

	if stringID == "" {
		err := errors.New("Missing Booking ID in path paramaters")
		ReturnFailure(c, 400, err)
		return nil, err
	}

	parsedID, parseErr := strconv.ParseInt(stringID, 10, 64)

	if parseErr != nil {
		err := errors.New("Invalid Booking ID given")
		ReturnFailure(c, 400, err)
		return nil, err
	}

	doctor, err := GetBooking(parsedID)

	if err != nil {
		ReturnFailure(c, 500, err)
		return nil, err
	}

	if doctor == nil {
		err := errors.New("Booking not found")
		ReturnFailure(c, 404, err)
		return nil, err
	}

	return doctor, nil
}

var doc = `{
  "basePath": "/",
  "info": {
    "contact": {
      "email": "z5061594@student.unsw.edu.au",
      "name": "API Support"
    },
    "description": "Service to make bookings and find availible times",
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "title": "Timeslot service",
    "version": "1.0"
  },
  "paths": {
    "/booking": {
      "get": {
        "consumes": [
          "application/json"
        ],
        "description": "Get all bookings, optionally filtering by doctor or start/endtimes. Returns a map of doctorID to booking map",
        "parameters": [
          {
            "description": "Bound the returned bookings by start time",
            "in": "query",
            "name": "startTime",
            "type": "string"
          },
          {
            "description": "Bound the returned bookings by end time",
            "in": "query",
            "name": "endTime",
            "type": "string"
          },
          {
            "description": "Return only bookings for this doctor",
            "in": "query",
            "name": "doctorID",
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DoctorIDtoBookingMap",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Get all bookings",
        "tags": [
          "Booking"
        ]
      },
      "post": {
        "consumes": [
          "application/json"
        ],
        "description": "Create a booking by providing booking info and timeslot information",
        "parameters": [
          {
            "description": "Timeslot and Booking information that should be assigned to the booking",
            "in": "body",
            "name": "BookingData",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NewBookingData",
              "type": "object"
            }
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Booking",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Create a booking",
        "tags": [
          "Booking"
        ]
      }
    },
    "/booking/{id}": {
      "delete": {
        "consumes": [
          "application/json"
        ],
        "description": "Deletes a booking, allowing the times to be rebooked",
        "parameters": [
          {
            "description": "Booking ID to be deleted",
            "in": "path",
            "name": "id",
            "required": true,
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DeleteBookingResponse",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Delete a booking",
        "tags": [
          "Booking"
        ]
      },
      "put": {
        "consumes": [
          "application/json"
        ],
        "description": "Updates the booking information of the given ID, creates a booking at that ID if one did not exist",
        "parameters": [
          {
            "description": "Booking ID to be modified",
            "in": "path",
            "name": "ID",
            "required": true,
            "type": "string"
          },
          {
            "description": "Timeslot and Booking information that should be updated to the booking",
            "in": "body",
            "name": "BookingData",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NewBookingData",
              "type": "object"
            }
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Booking",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Update a booking",
        "tags": [
          "Booking"
        ]
      }
    },
    "/timeslot": {
      "get": {
        "consumes": [
          "application/json"
        ],
        "description": "Returns the availible timeslots between the given start and end times. Defaults the start and end times to tomorrow 9-5",
        "parameters": [
          {
            "description": "Bound the returned timeslots by start time (default 9am tomorrow)",
            "in": "query",
            "name": "startTime",
            "type": "string"
          },
          {
            "description": "Bound the returned timeslots by end time (default 5pm tomorrow)",
            "in": "query",
            "name": "endTime",
            "type": "string"
          },
          {
            "description": "Return only timeslots for this doctor (default show all)",
            "in": "query",
            "name": "doctorID",
            "type": "string"
          },
          {
            "description": "Length of the requested timeslots (must be divisible by 30 minutes)",
            "in": "query",
            "name": "interval",
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DoctorIDtoTimeslotMap",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Get availible timeslots",
        "tags": [
          "Timeslot"
        ]
      }
    }
  },
  "swagger": "2.0",
  "definitions": {
    "ErrorResponse": {
      "type": "object",
      "properties": {
        "Error": {
          "type": "string"
        }
      }
    },
    "Timeslot": {
      "type": "object",
      "properties": {
        "DoctorID": {
          "type": "integer",
          "format": "int64"
        },
        "StartTime": {
          "type": "integer",
          "format": "int64"
        },
        "EndTime": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "BookingInfo": {
      "type": "object",
      "properties": {
        "ClientName": {
          "type": "string"
        },
        "Location": {
          "type": "string"
        },
        "Reason": {
          "type": "string"
        }
      }
    },
    "BookingMeta": {
      "type": "object",
      "properties": {
        "ID": {
          "type": "integer",
          "format": "int64"
        },
        "CreatedAt": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Booking": {
      "allOf": [
        {
          "$ref": "#/definitions/BookingInfo"
        },
        {
          "$ref": "#/definitions/BookingMeta"
        },
        {
          "$ref": "#/definitions/Timeslot"
        }
      ]
    },
    "NewBookingData": {
      "allOf": [
        {
          "$ref": "#/definitions/BookingInfo"
        },
        {
          "$ref": "#/definitions/Timeslot"
        }
      ]
    },
    "DeleteBookingResponse": {
      "type": "object",
      "properties": {
        "DeletedID": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "DoctorIDtoBookingMap": {
      "type": "object",
      "additionalProperties": {
        "$ref": "#/definitions/Booking"
      }
    },
    "DoctorIDtoTimeslotMap": {
      "type": "object",
      "additionalProperties": {
        "$ref": "#/definitions/Timeslot"
      }
    }
  }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo swaggerInfo

type s struct{}

func (s *s) ReadDoc() string {
	t, err := template.New("swagger_info").Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, SwaggerInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
