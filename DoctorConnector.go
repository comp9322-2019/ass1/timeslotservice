package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

const DoctorServiceRoot = "http://doctorservice.ap-southeast-2.elasticbeanstalk.com/doctor"

type DoctorListResp struct {
	Doctors []*Doctor
}

type Doctor struct {
	DoctorMeta
	DoctorInfo
}

type DoctorMeta struct {
	ID        int64
	CreatedAt int64
}

type DoctorInfo struct {
	FullName       string
	Location       string
	Specialisation string
}

func GetDoctorIDs() ([]int64, error) {
	resp, formErr := http.Get(DoctorServiceRoot)

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Encountered bad status code from Doctor service")
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	respParse := new(DoctorListResp)

	json.Unmarshal(buf.Bytes(), &respParse)

	toReturn := make([]int64, 1)

	for _, val := range respParse.Doctors {
		toReturn = append(toReturn, val.ID)
	}

	return toReturn, nil
}

func GetDoctor(ID int64) (*Doctor, error) {
	resp, formErr := http.Get(fmt.Sprintf("%s/%d", DoctorServiceRoot, ID))

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Encountered bad status code from Doctor service")
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	respParse := new(Doctor)

	json.Unmarshal(buf.Bytes(), &respParse)

	return respParse, nil
}
